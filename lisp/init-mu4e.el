;;; package --- Summary:
;;; Commentray:
;;; Code:

;;; try to locate mu4e
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

;;; soft require
(if (not (require 'mu4e nil t))
    ;; if true
    (message "mu4e not enabled.")

  ;; else
  (require 'smtpmail)
  (require 'mu4e-contrib)

  ;; send emails by msmtp
  (setq message-send-mail-function 'message-send-mail-with-sendmail
        sendmail-program "/usr/bin/msmtp"
        user-full-name "Wenjie Wang")

  ;; make mu4e default mail-user-agent
  (setq mail-user-agent 'mu4e-user-agent)

  ;; set default path for attachments
  (setq mu4e-attachment-dir  "~/Downloads")

  ;; keep cc myself
  (setq mu4e-compose-keep-self-cc t)

  ;; set date format
  (setq mu4e-date-format-long "%Y-%m-%d %H:%M:%S")
  (setq mu4e-headers-date-format "%m%d%y %H:%M")
  ;; mu4e only shows names in From: by default
  ;; we want the addresses
  (setq mu4e-view-show-addresses t)

  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)

  (setq mu4e-view-image-max-width 600)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))

  ;; does not need for mu v1.4
  ;; (setq mu4e-maildir (expand-file-name "~/email"))

  (setq message-send-mail-function 'smtpmail-send-it)
  (setq mu4e-contexts
        `(
          ,(make-mu4e-context
            :name "org"
            :enter-func (lambda () (mu4e-message "Entering wwenjie.org context"))
            :leave-func (lambda () (mu4e-message "Leaving wwenjie.org context"))
            :match-func (lambda (msg)
                          (when msg
                            (string-prefix-p
                             "/wang@wwenjie.org" (mu4e-message-field msg :maildir))))
            :vars '((mu4e-drafts-folder . "/wang@wwenjie.org/drafts")
                    (mu4e-sent-folder . "/wang@wwenjie.org/sent")
                    (mu4e-trash-folder . "/wang@wwenjie.org/trash")
                    (user-mail-address . "wang@wwenjie.org")
                    (smtpmail-default-smtp-server . "mail.privateemail.com")
                    (smtpmail-smtp-server . "mail.privateemail.com")
                    (smtpmail-smtp-user . "wang@wwenjie.org")
                    (smtpmail-stream-type . ssl)
                    (smtpmail-smtp-service . 465)
                    (mu4e-compose-signature
                     . (concat "Best,\nWenjie\n"))))

          ,(make-mu4e-context
            :name "Gmail"
            :enter-func (lambda () (mu4e-message "Entering Gmail context"))
            :leave-func (lambda () (mu4e-message "Leaving Gmail context"))
            :match-func (lambda (msg)
                          (when msg
                            (string-prefix-p
                             "/gmail" (mu4e-message-field msg :maildir))))
            :vars '((mu4e-drafts-folder . "/gmail/drafts")
                    (mu4e-sent-folder . "/gmail/sent")
                    (mu4e-trash-folder . "/gmail/trash")
                    (user-mail-address . "wjwang.stat@gmail.com")
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-user . "wjwang.stat@gmail.com")
                    (smtpmail-stream-type . starttls)
                    (smtpmail-smtp-service . 587)
                    (mu4e-compose-signature
                     . (concat "Best,\nWenjie\n"))))

          ,(make-mu4e-context
            :name "UConn"
            :enter-func (lambda () (mu4e-message "Entering UConn context"))
            :leave-func (lambda () (mu4e-message "Leaving UConn context"))
            :match-func (lambda (msg)
                          (when msg
                            (string-prefix-p
                             "/uconn" (mu4e-message-field msg :maildir))))
            :vars '((mu4e-drafts-folder . "/uconn/drafts")
                    (mu4e-sent-folder . "/uconn/sent")
                    (mu4e-trash-folder . "/uconn/trash")
                    (user-mail-address . "wenjie.2.wang@uconn.edu")
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-user . "wenjie.2.wang@uconn.edu")
                    (smtpmail-stream-type . starttls)
                    (smtpmail-smtp-service . 587)
                    ))

          )
        )

  ;; compose with the current context if no context matches;
  (setq mu4e-compose-context-policy nil)

  (setq mu4e-get-mail-command "mbsync -a"
        mu4e-html2text-command 'mu4e-shr2text
        mu4e-update-interval 600
        mu4e-headers-auto-update t
        mu4e-compose-signature-auto-include nil)

  ;; don't save message to Sent Messages, Gmail/IMAP takes care of this
  (setq mu4e-sent-messages-behavior 'delete)
  ;; show images
  (setq mu4e-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))

  ;; spell check
  (add-hook 'mu4e-compose-mode-hook
            (defun my-do-compose-stuff ()
              "My settings for message composition."
              (set-fill-column 72)
              (flyspell-mode)
              (save-excursion (message-add-header  "Cc: wang@wwenjie.org\n"))
              ))

  ;; shortcuts for jumpdir
  (setq mu4e-maildir-shortcuts
        '(("/wang@wwenjie.org/inbox" . ?i)
          ("/gmail/inbox" . ?g)
          ("/uconn/inbox" . ?u)
          ))

  ;; when replying, look kind of like gmail
  (setq message-citation-line-format "On %e %B %Y at %R %Z, %f wrote:\n")
  (setq message-citation-line-function 'message-insert-formatted-citation-line)

  ;; help prevent some uid errors
  (setq mu4e-change-filenames-when-moving t)

  ;; my key bindings
  (global-set-key (kbd "M-s m") 'mu4e)

  )


(provide 'init-mu4e)
;;; init-mu4e.el ends here

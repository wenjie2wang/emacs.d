;;; package --- Summary:

;;; Commentray:
;;; code:

;; set default file coding
;; (set-language-environment "UTF-8")
;; (set-default-coding-systems 'utf-8)

;; function that disables a minor mode globally
;; (defun global-disable-mode (mode-fn)
;;   "Disable `MODE-FN' in ALL buffers."
;;   (interactive "a")
;;   (dolist (buffer (buffer-list))
;;     (with-current-buffer buffer
;;       (funcall mode-fn -1))))

;; disable annoying prettify-symbols-mode
;; disable dimmer-mode.  I do not like it.
;; disable beacon-mode.  I do not like it.
;; disable company-quick-help. not a fan of popup.
(add-hook 'emacs-startup-hook (lambda ()
                                (global-prettify-symbols-mode -1)
                                (whole-line-or-region-global-mode -1)
                                (if (> emacs-major-version 24)
                                    (dimmer-mode -1))
                                (beacon-mode -1)
                                (anaconda-mode -1)
                                (company-quickhelp-mode -1)
                                ))

;;; disable origami-mode for folding
;;; it produces some error when using ggtags and I do not use it
(remove-hook 'prog-mode-hook 'origami-mode)
;;; disable line numbers, taking space
(remove-hook 'prog-mode-hook 'display-line-numbers-mode)
(remove-hook 'python-mode-hook 'anaconda-mode)

;;; delay company for using (e)shell in tramp
;; (defun delay-company-hook ()
;;   "Disable company idle completion."
;;   (setq-local company-idle-delay nil))
;; (add-hook 'shell-mode-hook 'delay-company-hook)
;; (add-hook 'eshell-mode-hook 'delay-company-hook)

;;; set default font
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-12"))

;;; enable line soft wrap
(defun enable-visual-line-hook ()
  "Use visual line mode."
  (visual-line-mode 1)
  (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))
  )
(add-hook 'latex-mode-hook 'enable-visual-line-hook)
;; (add-hook 'markdown-mode-hook 'enable-visual-line-hook)

;;; disable scroll-bars
(scroll-bar-mode -1)

;;; auto-refresh buffers
(global-auto-revert-mode t)

;; disable cua rectangle mark
(define-key cua-global-keymap [C-return] nil)

;; set package-archive-priorities
(if (> emacs-major-version 24)
    (setq package-archive-priorities
          '(("melpa-stable" . 5)
            ;; ("gnu"          . 3)
            ;; ("org"          . 1)
            ("melpa"        . 10)
            ))
  )

;; get rid of gaps
;; Non-nil means resize frames pixelwise.
;; If this option is nil, resizing a frame rounds its sizes to the frame's
;; current values of `frame-char-height' and `frame-char-width'.  If this
;; is non-nil, no rounding occurs, hence frame sizes can increase/decrease
;; by one pixel.
;; With some window managers you have to set this to non-nil in order to
;; fully maximize frames.  To resize your initial frame pixelwise,
;; set this option to a non-nil value in your init file.
(setq frame-resize-pixelwise t)

;;; for org mode
;; toggle C-TAB (org-force-cycle-archived) in org-mode
(eval-after-load "org"
  '(progn
     (define-key org-mode-map [C-tab] nil)))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
(setq org-agenda-files (list "~/git/org-my-life/org/"))
(setq org-reverse-note-order t)

;;; using firefox to browse url
(setq browse-url-browser-function 'browse-url-firefox
      browse-url-new-window-flag  t
      browse-url-firefox-new-window-is-tab t)

;; switch to other windows
;; (setq switch-window-shortcut-style 'qwerty)
;; (global-set-key [C-tab] 'switch-window)
;; (setq switch-window-minibuffer-shortcut ?z)

;;; use ace-window instead of switch window
(require-package 'ace-window)
(global-set-key (kbd "C-x o") 'ace-window)
;; (global-set-key (kbd "M-o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
(setq aw-background nil)

;; mimic the window-switch with the tab-switch in web browser
(global-set-key [C-S-tab] '(lambda () (interactive) (other-window -1)))
(global-set-key [C-tab] 'other-window)

;; set prefix for projectile
;; (define-key projectile-mode-map (kbd "M-s p") 'projectile-command-map)
(setq projectile-keymap-prefix (kbd "M-s p"))

;; For auto-fill-mode
(setq-default fill-column 80)

;; for tramp
(require 'tramp)
(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;;; speed up completions
(setq tramp-completion-reread-directory-timeout nil)
;;; disable version control to avoid delays:
;; (setq vc-ignore-dir-regexp
;;       (format "\\(%s\\)\\|\\(%s\\)"
;;               vc-ignore-dir-regexp
;;               tramp-file-name-regexp))
(setq vc-handled-backends '(Git))

;; automatically load theme, zenburn
;; (require-package 'zenburn-theme)
;; (load-theme 'zenburn t)
;; (setq-default custom-enabled-themes '(zenburn))
(setq-default custom-enabled-themes '(sanityinc-tomorrow-eighties))

;;; set global key for most programming languages
(global-set-key (kbd "M--") " = ")

;; require ESS
(require-package 'ess)
(require 'ess-site)
;; Turn off smart underscore in ESS
(setq ess-smart-S-assign-key nil)
;; ;; ESS Mode (.R file)
(setq ess-eval-visibly 'nowait)
(setq ess-history-file nil)
(setq ess-R-font-lock-keywords
      '((ess-R-fl-keyword:keywords . t)
        (ess-R-fl-keyword:constants  . t)
        (ess-R-fl-keyword:modifiers  . t)
        (ess-R-fl-keyword:fun-defs   . t)
        (ess-R-fl-keyword:assign-ops . t)
        (ess-fl-keyword:fun-calls . t)
        (ess-fl-keyword:numbers)
        (ess-fl-keyword:operators . t)
        (ess-fl-keyword:delimiters)
        (ess-fl-keyword:= . t)
        (ess-R-fl-keyword:F&T . t)))
(setq inferior-R-font-lock-keywords
      '((ess-S-fl-keyword:prompt . t) ;; comint is bad at prompt highlighting
        (ess-R-fl-keyword:keywords . t)
        (ess-R-fl-keyword:constants . t)
        (ess-R-fl-keyword:modifiers . t)
        (ess-R-fl-keyword:messages . t)
        (ess-R-fl-keyword:fun-defs . t)
        (ess-R-fl-keyword:assign-ops . t)
        (ess-fl-keyword:matrix-labels . t)
        (ess-fl-keyword:fun-calls . t)
        (ess-fl-keyword:numbers)
        (ess-fl-keyword:operators . t)
        (ess-fl-keyword:delimiters)
        (ess-fl-keyword:= . t)
        (ess-R-fl-keyword:F&T . t)))
(setq inferior-R-args "--no-save --no-restore --no-site-file --no-environ")
(eval-after-load "ESS"
  '(progn
     (define-key ess-mode-map (kbd "\C-r") 'ess-eval-function-or-paragraph-and-step)
     (define-key ess-mode-map (kbd "M--") " <- ")
     (define-key ess-mode-map (kbd "M-0 M--") nil)
     (define-key inferior-ess-mode-map (kbd "M--") " <- ")
     (define-key inferior-ess-mode-map (kbd "M-0 M--") nil)
     ;;; also use `find-tag' from etags.el
     (define-key ess-mode-map (kbd "C-c M-.") 'find-tag)
     ))

;;; require julia-mode
(require-package 'julia-mode)

;; (require 'auto-complete-config)
;; (ac-config-default)

;; (require 'autopair)
;; (autopair-global-mode) ;; enable autopair in all buffers

;;; try column-enforce-mode
(require-package 'column-enforce-mode)
(add-hook 'ess-mode-hook (lambda () (interactive) (column-enforce-mode)))
(add-hook 'c++-mode-hook (lambda () (interactive) (column-enforce-mode)))

;;; disable flymake and flycheck for ess
(add-hook 'ess-mode-hook
          (lambda ()
            (setq ess-use-flymake nil)
            (setq flycheck-disabled-checkers '(r-lintr))
            )
          )

;;; enable etags backend in addition to ess-r-xref-backend
;;; modified from Pan Xie's example at https://www.emacswiki.org/emacs/TagsFile
(when (require 'xref nil t)
  (defun xref--find-xrefs (input kind arg display-action)
    "Re-define the `xref--find-xrefs'.
 This is a re-defined version of `xref--find-xrefs'.  This
 function will call all backends until the definitions are found
 or the `xref-backend-functions' is exhausted."
    (let ((fn (intern (format "xref-backend-%s" kind)))
          (tail xref-backend-functions))
      (cl-block nil
        (while tail
          (let* ((backend-fn (car tail))
                 (backend (and (symbol-function backend-fn) (funcall backend-fn)))
                 (xrefs (and backend (funcall fn backend arg))))
            (when xrefs
              (cl-return (xref--show-xrefs xrefs display-action))))
          (setq tail (cdr tail)))
        (user-error "No %s found for: %s" (symbol-name kind) input)))))

;; add a hook function to the `ess-mode-hook':
(defun hook:ess-mode ()
  "Hook for ess-mode."
  (when (require 'xref nil t)
    (setq-local xref-backend-functions '(etags--xref-backend ess-r-xref-backend t))))
(add-hook 'ess-mode-hook 'hook:ess-mode)

;; require polymode
(require-package 'polymode)
(require-package 'poly-markdown)
(require-package 'poly-R)
(require-package 'poly-noweb)
(require 'poly-markdown)
(require 'poly-R)
(require 'poly-noweb)

;;; insert code chunk
;;; https://emacs.stackexchange.com/questions/27405/insert-code-chunk-in-r-markdown-with-yasnippet-and-polymode
(defun rmd-insert-r-chunk (header)
  "Insert an r-chunk HEADER in markdown mode.
Necessary due to interactions between polymode and yas snippet."
  (interactive "sHeader: ")
  (insert (concat "```{r " header "}\n\n```"))
  (forward-line -1))


(custom-set-variables
 '(markdown-command "/usr/bin/pandoc"))

;; (autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
;; (autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
;; (autoload 'tex-mode-flyspell-verify "flyspell" "" t)
(add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(setq flyspell-sort-corrections nil)
(setq flyspell-doublon-as-error-flag nil)
(setq flyspell-issue-message-flag nil)

;; using auctex
(require-package 'auctex)
;; Turn on RefTeX in AUCTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
;; Activate nice interface between RefTeX and AUCTeX
(setq reftex-plug-into-AUCTeX t)
;;; add company backend for latex math
(require-package 'company-math)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-math-symbols-unicode))

;; ;; web-mode
;; (require-package 'web-mode)
;; (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

;; matlab-mode
;; (require-package 'matlab-mode)
;; (autoload 'matlab-mode "matlab" "Matlab Editing Mode" t)
;; (add-to-list
;;  'auto-mode-alist
;;  '("\\.m$" . matlab-mode))
;; (setq matlab-indent-function t)
;; (setq matlab-shell-command "matlab")

;; python-mode
;; (require-package 'python-mode)
;; (autoload 'python-mode "python-mode" "Python Mode." t)
;; (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
;; (add-to-list 'interpreter-mode-alist '("python" . python-mode))
;; (eval-after-load "python-mode"
;;   '(progn
;;      (define-key python-mode-map [(kbd "C-RET")] 'py-execute-line)
;;      (define-key python-mode-map [(kbd "\C-r")] 'py-execute-region)))

;;; flycheck with pylint
(setq flycheck-python-pylint-executable "pylint")

;; elpy for python
(require-package 'elpy)
(elpy-enable)

;;; enable ein, emacs-ipython-notebook
;; (require-package 'ein)
;; (require 'ein)

;;; using indium for dev of javascript code
;; (require-package 'indium)

;;; using ivy-bibtex
(require-package 'ivy-bibtex)
(global-set-key (kbd "M-s b") 'ivy-bibtex)
(setq bibtex-completion-bibliography
      '("~/texmf/bibtex/bib/index.bib"))
(setq bibtex-completion-library-path
      '("~/bibrary/papers"
        "~/bibrary/books"
        "~/bibrary/misc"))
;;; I do not use notes anyway
;; (setq bibtex-completion-notes-path
;;       "~/bibrary/notes.org")
(setq bibtex-completion-notes-path nil)
(setq bibtex-completion-pdf-field nil)
(setq bibtex-completion-find-additional-pdfs t)
(setq bibtex-completion-pdf-extension '(".pdf" ".djvu"))
(setq bibtex-completion-pdf-open-function
      (lambda (fpath)
        (call-process "okular" nil 0 nil fpath)))

;;; using gsholar-bibtex
(require-package 'gscholar-bibtex)

;;; auto run bib-validate-globally whenever saving bibtex files
(defun bibtex-auto-validate-globally ()
  "Run bibtex-validate-globally after saving the bibtex files."
  (when (eq major-mode 'bibtex-mode)
    (bibtex-validate-globally)))
(add-hook 'after-save-hook #'bibtex-auto-validate-globally)

;;; using imenu-anywhere
(require-package 'imenu-anywhere)
;;; add a mark before running imenu-anywhere so that it is possible to go back
;;; by C-u C-Space
(global-set-key (kbd "C-.") (lambda () (interactive)
                              (cua-set-mark)
                              (cua-set-mark)
                              (imenu-anywhere)
                              ))

;;; using ggtags
(require-package 'ggtags)
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))


;;; using semantic for auto-complete
;; (require 'semantic)
;; (semantic-mode 1)
;; (setq semantic-default-submodes
;;       '(;; Perform semantic actions during idle time
;;         global-semantic-idle-scheduler-mode
;;         ;; Use a database of parsed tags
;;         global-semanticdb-minor-mode
;;         ;; Decorate buffers with additional semantic information
;;         ;; global-semantic-decoration-mode
;;         ;; Highlight the name of the function you're currently in
;;         ;; global-semantic-highlight-func-mode
;;         ;; show the name of the function at the top in a sticky
;;         ;; global-semantic-stickyfunc-mode
;;         ;; Generate a summary of the current tag when idle
;;         ;; global-semantic-idle-summary-mode
;;         ;; Show a breadcrumb of location during idle time
;;         ;; global-semantic-idle-breadcrumbs-mode
;;         ;; Switch to recently changed tags with `semantic-mrub-switch-tags',
;;         ;; or `C-x B'
;;         ;; global-semantic-mru-bookmark-mode
;;         ))

;;; using another indentation style
(c-add-style "ellemtel-offset4"
             '("ellemtel"
               (indent-tabs-mode . nil)        ; use spaces rather than tabs
               (c-basic-offset . 4)            ; indent by four spaces
               (c-indent-level . 4)
               ))
(defun my-c++-mode-hook ()
  "Use my-style defined above."
  (c-set-style "ellemtel-offset4")
  )
(add-hook 'c++-mode-hook 'my-c++-mode-hook)

;;; use c++-mode by default for .h files
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;;; require irony
(require-package 'irony)
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(defun my-irony-mode-hook ()
  "Update the `completion-at-point' and `complete-symbol' bindings."
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)

;;; use irony eldoc
(require-package 'irony-eldoc)
(add-hook 'irony-mode-hook #'irony-eldoc)

(defun my-company-irony ()
  "Avoid enabling irony-mode in modes that inherits 'c-mode', e.g: 'php-mode'."
  (when (eq major-mode 'c-mode)
    (irony-mode)
    (unless (memq 'company-irony company-backends)
      (setq-local company-backends (cons 'company-irony company-backends)))))

;;; using company-irony and company-irony-c-headers
(require-package 'company-irony)
(require-package 'company-irony-c-headers)
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)
(setq company-backends (delete 'company-semantic company-backends))
(eval-after-load 'company
  '(add-to-list 'company-backends '(company-irony company-irony-c-headers)))
;; (setq company-idle-delay 0.5)

;;; flycheck
(add-hook 'c++-mode-hook
          (lambda ()
            (setq flycheck-gcc-language-standard "c++11")
            ;; (setq flycheck-disabled-checkers '(c/c++-clang))
            ;; (setq flycheck-select-checker "c/c++-gcc")
            (setq flycheck-select-checker "c/c++-clang")
            ;; add R, Rcpp, and its friends
            (setq flycheck-clang-include-path
                  (list
                   "/usr/include/R/"
                   "/usr/local/lib/R/site-library/Rcpp/include"
                   "/usr/local/lib/R/site-library/RcppArmadillo/include"
                   "/usr/local/lib/R/site-library/RcppEigen/include"
                   )
                  )
            ))

;;; flycheck-irony seems to miss warnings such as unused variables
;;; and need to set up .clang_complete for Rcpp
;;; so it is disabled now in favor of native flycheck
;; (require-package 'flycheck-irony)
;; (eval-after-load 'flycheck
;;   '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;;; enable stan-mode
;; (require-package 'stan-mode)

;;; fix from https://github.com/bbatsov/projectile/issues/835
(add-hook 'text-mode-hook 'projectile-mode)

;;; pdf-tools, nice but obviously slower than okular
(require-package 'pdf-tools)
;; (pdf-tools-install)

;;; optional config for mu4e
(require 'init-mu4e nil t)
;; config for doxygen
(require 'init-doxygen nil t)

;;; add org-export to twbs
;; (require-package 'ox-twbs)

;;; enable eyebrowse
(require-package 'eyebrowse)
(eyebrowse-mode t)

;;; using elfeed
;; (require-package 'elfeed)
;; (global-set-key (kbd "M-s n") 'elfeed)
;; (setq
;;  elfeed-feeds
;;  '("https://gitlab.com/dashboard/projects.atom?feed_token=vYLutpvX-Ew6RAsixpim"
;;    "https://github.com/wenjie2wang.private.atom?token=AKkY45xfLZa6RXKn_9o2LBOQFzLC1FKXks65XpW9wA=="))
;; (setf url-queue-timeout 30)

;;; use graphviz-dot-mode
(require-package 'graphviz-dot-mode)
(setq graphviz-dot-indent-width 4)
(setq graphviz-dot-auto-indent-on-semi nil)
(setq electric-graphviz-dot-open-brace nil)
(setq electric-graphviz-dot-close-brace nil)

;;; set up unicode (not so necessary now)
;; (require-package 'unicode-fonts)
;; (unicode-fonts-setup)

;;; use org-recoll
(require 'init-recoll nil t)

;;; use yasnippet-snippets
(require-package 'yasnippet-snippets)
(yas-global-mode 1)

;;; Chinese input
(require-package 'pyim)
(require-package 'pyim-basedict)
(pyim-basedict-enable)
(setq default-input-method "pyim")
(setq pyim-page-length 10)
;;; fuzzy pinyin
(setq pyim-fuzzy-pinyin-alist
      '(("c" "ch")
        ("s" "sh")
        ("z" "zh")
        ("en" "eng")
        ("in" "ing"))
      )

;;; docker file
(require-package 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))

;;; use vterm
(require-package 'vterm)
;;; define shortcut for vterm-yank
(defun set-vterm-yank ()
  "Set shortcut for vterm yank in vterm"
  (define-key vterm-mode-map (kbd "C-y") 'vterm-yank))
(add-hook 'vterm-mode-hook #'set-vterm-yank)


(provide 'init-local)
;;; init-local.el ends here

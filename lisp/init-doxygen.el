;;; package --- Summary:
;;; Commentray:
;;; Code:

;; highlight doxygen documentation

;; I personally prefer consistently using `//!' for all my function
;; documentation.
;; customized from the reference

;; https://emacs.stackexchange.com/questions/
;; 31392/syntax-highlight-doxygen-comments-in-c-c-c-doc-comment-style

;; Generic doxygen formatting
(eval-after-load "c++-mode"
  '(progn
     (defconst custom-font-lock-doc-comments
       (let ((symbol "[a-zA-Z0-9_]+"))
         `((,(concat "</?\\sw"             ; HTML tags.
                     "\\("
                     (concat "\\sw\\|\\s \\|[=\n\r*.:]\\|"
                             "\"[^\"]*\"\\|'[^']*'")
                     "\\)*>")
            0 ,c-doc-markup-face-name prepend nil)
           (,(concat "\'" symbol "\'")     ; 'symbol'
            0 ,c-doc-markup-face-name prepend nil)
           (,(concat "\`" symbol "\`")     ; `symbol`
            0 ,c-doc-markup-face-name prepend nil)
           (,(concat "\`\`" symbol "\`\`") ``symbol``
            0 ,c-doc-markup-face-name prepend nil)
           (,(concat
              "[\\@]" ;; start of Doxygen special command
              "\\(?:"
              "[a-z]+\\|"    ;; typical word Doxygen special @cmd or \cmd
              "[[:punct:]]+" ;; non-word commands ( \{ \# ...etc)
              "\\)")
            0 ,c-doc-markup-face-name prepend nil)

           (,(concat "#" symbol)           ; #some_c_synbol
            0 ,c-doc-markup-face-name prepend nil)
           )
         ))
     ;; Matches across multiple lines:
     ;;   /** doxy comments */
     ;;   /*! doxy comments */
     ;;   /// doxy comments
     ;;   //! doxy comments
     ;; Doesn't match:
     ;;   /*******/
     (defconst custom-font-lock-keywords
       `((,(lambda (limit)
             (c-font-lock-doc-comments "/\\(//\\|/!\\|\\*[\\*!][^\\*!]\\)"
                                       limit custom-font-lock-doc-comments)))))
     (setq-default c-doc-comment-style (quote (custom)))
     )
  )

;;; https://www.emacswiki.org/emacs/DoxyMacs
;; Advanced return for programming.
(defun advanced-return ()
  "Advanced `newline' command for comment.  This function
redefine <Enter> to provide a corrent comment symbol at each
newline plus a space when you press <Enter> in the comment.  It
also support JavaDoc style comment -- insert a `*' at the
beggining of the new line if inside of a comment."
  (interactive "*")
  (let* ((last (point))
         (line-beginning (progn (beginning-of-line) (point)))
         (is-inside-java-doc
          (progn
            (goto-char last)
            (if (search-backward "*/" nil t)
                ;; there are some comment endings - search forward
                (search-forward "/*" last t)
              ;; it's the only comment - search backward
              (goto-char last)
              (search-backward "/*" nil t)
              )
            )
          )
         (is-inside-oneline-comment
          (progn
            (goto-char last)
            (search-backward "//!" (line-beginning-position) t)
            )
          )
         )

    ;; go to last char position
    (goto-char last)

    ;; the point is inside one line comment, insert the comment-start.
    (if is-inside-oneline-comment
        (progn
          (newline-and-indent)
          (insert "//! ")
          )
      ;; else we check if it is java-doc style comment.
      (if is-inside-java-doc
          (progn
            (newline-and-indent)
            (insert "* ")
            )
        ;; else insert only new-line
        (newline-and-indent)
        )
      )
    )
  )

(add-hook 'c++-mode-hook
          (lambda ()
            (local-set-key (kbd "<RET>") 'advanced-return)))


(provide 'init-doxygen)
;;; init-doxygen.el ends here

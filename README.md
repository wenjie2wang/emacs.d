# About

This repository contains Wenjie's personal Emacs configuration built upon
Steve Purcell's configuration: <https://github.com/purcell/emacs.d>.

**Obsolete**: I have rewritten a new `.emacs.d` for my own following this setup
in [another repository](https://gitlab.com/wenjie2wang/dot-emacs-dot-d) to learn
more about using Emacs.  No updates will be pushed to this repository.
